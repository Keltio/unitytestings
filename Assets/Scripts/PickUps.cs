﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUps : MonoBehaviour
{
    public StatsScript stats;

    private void OnTriggerEnter(Collider other)
    {
        stats.score += 50;
        this.gameObject.SetActive(false);
        stats.pickedUp += 1;
    }
}

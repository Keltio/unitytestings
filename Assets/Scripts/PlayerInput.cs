﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    public int position = 1;
    float moveDistance = 5;
    public Rigidbody rb;
    public bool onTheGround;
    public int jumpForce;
    public float timePassed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timePassed += Time.deltaTime;
        if (timePassed > 2)
        {
            onTheGround = true;
        }

        if (Input.GetKeyDown(KeyCode.A))
        {
            Left();
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            Right();
        }
        if (Input.GetKeyDown(KeyCode.Space)&& onTheGround)
        {
            Jump();
        }

    }

    public void Left()
    {
        if (position == 1)
        {
            transform.Translate(-moveDistance, 0, 0);
            position = 0;
        }
        else if (position == 2)
        {
            transform.Translate(-moveDistance, 0, 0);
            position = 1;
        }
        else if (position == 0)
        {
            transform.Translate(moveDistance * 2, 0, 0);
            position = 2;
        }
    }

    public void Right()
    {

        if (position == 0)
        {
            transform.Translate(moveDistance, 0, 0);
            position = 1;
        }
        else if (position == 1)
        {
            transform.Translate(moveDistance, 0, 0);
            position = 2;
        }
        else if (position == 2)
        {
            transform.Translate(-moveDistance * 2, 0, 0);
            position = 0;
        }
    }

    public void Jump()
    {
        rb.velocity = Vector3.up * jumpForce;
        timePassed = 0;
        onTheGround = false;
    }


}

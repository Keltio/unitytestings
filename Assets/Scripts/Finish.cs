﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Finish : MonoBehaviour
{
    public StatsScript stats;
    private void OnTriggerEnter(Collider other)
    {
        stats.finished++;
        stats.Restart();
    }
}

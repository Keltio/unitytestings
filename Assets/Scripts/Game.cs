﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    public GameObject game;

    private void Awake()
    {
        game = Resources.Load("Prefabs/Game") as GameObject;
    }
    // Start is called before the first frame update
    void Start()
    {
        Instantiate(game);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

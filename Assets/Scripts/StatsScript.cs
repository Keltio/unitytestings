﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class StatsScript : MonoBehaviour
{
    public int lives = 3;
    public int score = 0;
    public int distance = 0;
    public int pickedUp=0;
    public int finished, deaths;
    public GameObject level;
    public Text displayScore;
    public Text displayLives;
    public Text displayPickups;
    public Text displayDistance;
    public bool restart;
    public float timePassed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(lives == 0)
        {
            Restart();
        }


        timePassed += Time.deltaTime;

        displayScore.text = "Current Score: " + score.ToString();
        displayLives.text = "Current Lives: " + lives.ToString();
        displayDistance.text = "Current Distance: " + distance.ToString();
        displayPickups.text = "Current pickups picked: " + pickedUp.ToString();
    }

    public void Restart()
    {
        lives = 3;
        score = 0;
        timePassed = 0;
        restart = true;
        pickedUp = 0;
        distance = 0;
        level.transform.position = new Vector3(0,0,0);

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public class LevelMovement : MonoBehaviour
    {
        public Rigidbody rb;
        public float movement = -10;
        public StatsScript stats;
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (stats.restart == false)
                rb.velocity = new Vector3(0, 0, movement);
        }
        private void FixedUpdate()
        {
            if (stats.restart == false)
            {
                stats.distance += 1;
                stats.score += 1;
            }

            if (stats.timePassed >= 3)
            {
                movement -= 0.3f;
                stats.timePassed = 0;
                stats.restart = false;
            }
        }
    }
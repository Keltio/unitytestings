﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleMovement : MonoBehaviour
{
    public float timePassed;
    public int position;
    int moveDistance = 5;
    int random;
    private void FixedUpdate()
    {
        random = Random.Range(0, 1);
        timePassed += Time.deltaTime;
        if (timePassed >= 2)
        {
            
            if (random==0)
            {
                if (position == 1)
                {
                    transform.Translate(-moveDistance, 0, 0);
                    position = 0;
                }
                else if (position == 2)
                {
                    transform.Translate(-moveDistance, 0, 0);
                    position = 1;
                }
                else if (position == 0)
                {
                    transform.Translate(moveDistance * 2, 0, 0);
                    position = 2;
                }
            }

            if (random==1)
            {
                if (position == 0)
                {
                    transform.Translate(moveDistance, 0, 0);
                    position = 1;
                }
                else if (position == 1)
                {
                    transform.Translate(moveDistance, 0, 0);
                    position = 2;
                }
                else if (position == 2)
                {
                    transform.Translate(-moveDistance * 2, 0, 0);
                    position = 0;
                }
            }
            timePassed = 0;
        }
    }
}

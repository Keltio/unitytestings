﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class PlayTest
    {
        StatsScript stats;
        PlayerInput input;
        GameObject game;
        GameObject player;

        [SetUp]
        public void SetUp()
        {
            game = MonoBehaviour.Instantiate(Resources.Load<GameObject>("Prefabs/Game"));
        }


        [TearDown]
        public void Teardown()
        {
          Object.Destroy(game);
        }
        
        [UnityTest]
        public IEnumerator PlayerDeaths()
        {
            player = GameObject.Find("Player");
            input = player.GetComponent<PlayerInput>();
            stats = GameObject.Find("Scriptholder").GetComponent<StatsScript>();
            yield return new WaitForSeconds(2f);
            input.Left();
            yield return new WaitForSeconds(6f);
            input.Right();
            yield return new WaitForSeconds(10f);
            Assert.Less(stats.score, 500);
        }
        
        [UnityTest]
        public IEnumerator PlayerFinished()
        {
            player = GameObject.Find("Player");
            input = player.GetComponent<PlayerInput>();
            stats = GameObject.Find("Scriptholder").GetComponent<StatsScript>();
            stats.Restart();
            yield return new WaitForSeconds(8f);
            input.Left();
            yield return new WaitForSeconds(6f);
            input.Jump();
            yield return new WaitForSeconds(2f);
            input.Jump();
            yield return new WaitForSeconds(3f);
            input.Jump();
            yield return new WaitForSeconds(1.5f);
            input.Right();
            yield return new WaitForSeconds(5f);
            input.Jump();
            yield return new WaitForSeconds(7f);
            input.Jump();
            yield return new WaitForSeconds(6f);
            input.Jump();
            Debug.Log("8");
            yield return new WaitForSeconds(4f);
            input.Jump();
            Debug.Log("9");
            yield return new WaitForSeconds(6f);
            input.Left();
            yield return new WaitForSeconds(2.5f);
            input.Right();
            yield return new WaitForSeconds(8f);
            Debug.Log(stats.finished);
            Assert.Greater(stats.finished, 0);
        }
    }
}

@echo off

SET UNITYVERSION=2020.1.6f1
IF NOT [%1]==[] (set UNITYVERSION=%1)

SET PRODUCTNAME="TheGame"
IF NOT [%2]==[] (set PRODUCTNAME=%2)

SET COMPANYNAME="Company Name"
IF NOT [%3]==[] (set COMPANYNAME=%3)

SET TARGET=Windows
IF NOT [%4]==[] (set TARGET=%4)

SET VERSION=0.0.0.1
IF NOT [%5]==[] (set VERSION=%5)

SET BUILDLOCATION="./Build/%TARGET%/%VERSION%"

rmdir -S %BUILDLOCATION%
mkdir %BUILDLOCATION%

>buildManifest.txt (
    echo ProductName=%PRODUCTNAME%
    echo CompanyName=%COMPANYNAME%
    echo Version=%VERSION%
    echo BuildLocation=%BUILDLOCATION%
)

C:\"Program Files"\Unity\Hub/Editor\2020.1.6f1\Editor\Unity.exe -quit -batchMode -projectPath C:\Users\mulde\Documents\Afstuderen\UnitTesting\UnityTestingNew -executeMethod BuildHelper.Windows

del /f buildManifest.txt